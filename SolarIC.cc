using namespace std;
#include<iostream>
#include<valarray>
#include<vector>
#include<string>
#include<cstring>

#include"SolarIC.h"
#include"StarPhotonField.h"
#include"InverseCompton.h"
#include"LeptonSpectrum.h"


#include"fitsio.h"

///////////////////////////////////////
void SolarIC::init(double Egamma_min_, double Egamma_max_, double Egamma_factor_, 
	           double     Ee_min_, double     Ee_max_, double     Ee_factor_,
                   double    Eph_min_, double    Eph_max_, double    Eph_factor_,
                   double  theta_min_, double  theta_max_, double     dtheta_,    string theta_grid_type_,
                   double      s_min_, double      s_max_, double     ds_,
                   double      r_min_, double      r_max_,
                   double      Tstar_, double      Rstar_, double      d_,          
                   string   leptons_name_,        valarray<double> leptons_parameters_,
                   int      IC_emiss_method_,
                   string   FITS_output_filename_,
                   int debug_)

{
 cout<<">>SolarIC::init"<<endl;

#include"Constants.h"

   Egamma_min   = Egamma_min_;
   Egamma_max   = Egamma_max_;
   Egamma_factor= Egamma_factor_;

   Ee_min       = Ee_min_;
   Ee_max       = Ee_max_;
   Ee_factor    = Ee_factor_;

   Eph_min      = Eph_min_;
   Eph_max      = Eph_max_;
   Eph_factor   = Eph_factor_;

   theta_min       = theta_min_;
   theta_max       = theta_max_;
   dtheta          = dtheta_;
   
    
   s_min     = s_min_;
   s_max     = s_max_;
   ds        =ds_;

   r_min     = r_min_;
   r_max     = r_max_;

 

   Tstar = Tstar_;           // solar temperature, K
   Rstar = Rstar_;            // solar radius,      cm
   d     = d_;                // Sun is normally at 1.0 AU


   leptons_name=             leptons_name_;
   leptons_parameters.resize(leptons_parameters_.size());
   leptons_parameters=       leptons_parameters_;

   IC_emiss_method = IC_emiss_method_;

   FITS_output_filename= FITS_output_filename_;

   debug=debug_;


  int n_grid = int(log(Egamma_max/Egamma_min)/log(Egamma_factor) + 1.001);
  Egamma.resize(n_grid);
  for(int i=0;i<Egamma.size();i++) Egamma[i]= Egamma_min*pow(Egamma_factor,i);

      n_grid = int(log(Ee_max/Ee_min)/log(Ee_factor)+ 1.001) ;
  Ee.resize(n_grid);
  for(int i=0;i<Ee.size();i++)     Ee[i]    = Ee_min    *pow(Ee_factor,    i);

      n_grid = int(log(Eph_max/Eph_min)/log(Eph_factor) + 1.001) ;
  Eph.resize(n_grid);
  for(int i=0;i<Eph.size();i++)    Eph[i]   = Eph_min   *pow(Eph_factor,   i);

  cout<<"gamma grid size="<<Egamma.size()<<endl;
  cout<<"Egamma= ";for(int i=0;i<Egamma.size();i++)cout<<Egamma[i]<<" " ; cout<<endl;

  cout<<"electron grid size="<<Ee.size()<<endl;
  cout<<"Ee= ";    for(int i=0;i<Ee.size();i++)    cout<<    Ee[i]<<" " ; cout<<endl;

  cout<<"target photon grid size="<<Eph.size()<<endl;
  cout<<"Eph= ";   for(int i=0;i<Eph.size();i++)   cout<<   Eph[i]<<" " ; cout<<endl;


  theta_grid_type=theta_grid_type_;

  if(theta_grid_type!="linear" && theta_grid_type!="log") theta_grid_type="linear"; // in case wrongly defined
  cout<<"theta grid type="<<theta_grid_type<<endl;

  if(theta_grid_type=="linear")  n_grid = int((theta_max-theta_min)/dtheta + 1.001) ;
  if(theta_grid_type=="log"   )  n_grid = int((log(theta_max)-log(theta_min))/log(1.+dtheta) + 1.001) ;

  theta.resize(n_grid);
                                 
  if(theta_grid_type=="linear")  for(int i=0;i<theta.size();i++)  theta[i]=theta_min + dtheta*i;
  if(theta_grid_type=="log"   )  for(int i=0;i<theta.size();i++)  theta[i]=exp( log(theta_min) + log(1.+dtheta)*i );

  cout<<"theta grid size="<<theta.size()<<endl;
  cout<<"theta= ";   for(int i=0;i<theta.size();i++)   cout<<   theta[i]<<" " ; cout<<endl;

  cout<<"  leptons_name: "<<leptons_name;
  cout<<"  leptons_parameters: ";
  for(int i=0;i<  leptons_parameters.size();i++) cout<<  leptons_parameters[i]<<" " ; cout<<endl;

  cout<<"  FITS_output_filename: "<<FITS_output_filename<<endl;

  cout<<"<<SolarIC::init"<<endl;
  return;
}
///////////////////////////////////////
void SolarIC::compute()
{
  cout<<">> SolarIC::compute"<<endl;

#include"Constants.h"


  double dtr=pi/180.;

  int    debug_inverse_compton  =0;
  int    debug_star_photon_field=0;
  int    debug_lepton_spectrum  =0;

 StarPhotonField starphotonfield;
 starphotonfield.init(Tstar,Rstar,debug_star_photon_field);
 cout<<"Tstar="<<starphotonfield.T<<" Rstar="<<starphotonfield.Rstar<<endl;
  

 if(debug==1)
 {
  double r=1e12;
  cout<<"testing nph, r="<<r<<endl;
  valarray<double> nBB,nph;
  nBB.resize(Eph.size());
  nph.resize(Eph.size());
  nBB=starphotonfield.nBB  (Eph  );// just to test, not used
  nph=starphotonfield.nph  (Eph,r);// just to test, not used
 
  for(int i=0;i<Eph.size();i++) cout<<"SolarIC::compute: i="<<i<<" Eph="<<Eph[i]<<" nBB="<<nBB[i]<<" nph="<<nph[i]<<" "<<endl;
 }

 //  valarray<double>   sum(Egamma.size());
 // valarray<double> emiss(Egamma.size());
 

 //  valarray<double> fluxe(Ee.size());

  intensity                  .resize(theta.size());
  intensity_integrated_E     .resize(theta.size());
  intensity_integrated_theta .resize(theta.size());
  intensity_integrated_theta_E.resize(theta.size());

  for(int itheta=0;itheta<theta.size();itheta++)
  {
   intensity                   [itheta].resize(Egamma.size());
   intensity_integrated_E      [itheta].resize(Egamma.size());
   intensity_integrated_theta  [itheta].resize(Egamma.size());
   intensity_integrated_theta_E[itheta].resize(Egamma.size());
  }


  
  LeptonSpectrum leptonspectrum;
  
  leptonspectrum.init(  leptons_name,  leptons_parameters,debug_lepton_spectrum);


  InverseCompton inversecompton;
                 inversecompton.init(debug_inverse_compton);

 double theta_star=asin(Rstar/(d*AUcm))/dtr; // angle subtended by star

 cout<<"SolarIC::compute: s_min="<<s_min<<" s_max="<<s_max<<" ds="<<ds
      <<" r_min="<<r_min<<" r_max="<<r_max<< " theta_star="<<theta_star
      <<" IC_emiss_method="<<IC_emiss_method<<endl;

 // d, r, s in AU  

 // declare private variables inside loop, so do not need private clause
 
#pragma omp parallel for schedule(runtime)   default(shared)

 for(int itheta=0;itheta<theta.size();itheta++)
 {
   cout<<"SolarIC:: compute: theta="<<theta[itheta]<<endl;

  
  double                          s_max_star = s_max;
  if (theta[itheta] < theta_star) s_max_star = d; // maximum considering stellar occultation

  valarray<double>   sum(Egamma.size());// inside loop for openmp, private does not have desired effect for valarrays
  valarray<double> emiss(Egamma.size());
  valarray<double> fluxe(Ee    .size());
  valarray<double>   nph(Eph   .size());

  sum=0;
  double s;
  for(s=s_min;s<s_max_star;s+=ds)
  {
    

  double r =sqrt(s*s + d*d - 2.0*s*d*cos(theta[itheta]*dtr) );

 
  double rcm=r*AUcm;

  

   if(debug==1)
   cout<<"SolarIC:: compute: theta="<<theta[itheta] <<" s="<<s<<" r="<<r<<endl;

   
 if(r>r_min && r<r_max)
 {  

  double eta; // 0= head on collision
  double         coseta=( r*r + s*s -d*d )/(2.*r*s);
  if(coseta> 1.0)coseta= 1.0;// catch numerical accuracy
  if(coseta<-1.0)coseta=-1.0;// catch numerical accuracy
        eta=acos(coseta);
  
 
  fluxe=leptonspectrum.fluxe(Ee,r);


  nph=starphotonfield.nph  (Eph,rcm);

  inversecompton.IC_emiss(Ee     , Eph     , eta, Egamma,
                          fluxe,   nph,           emiss,
                          IC_emiss_method  );
  
  sum+=emiss;

  if(debug==1)
  for (int i=0;i<Egamma.size();i++) cout<<"SolarIC:: compute: theta="<<theta[itheta]
       <<" s="<<s<<" r="<<r<<" eta="<<eta<<" Egamma= "<< Egamma[i]<<" emiss="<<emiss[i]<<" sum="<<sum[i]<<endl;
      

  }//if r>r_min, r<r_max

 }//s

  sum*=ds*AUcm;

  if(debug==1)
  for (int i=0;i<Egamma.size();i++) cout<<"SolarIC:: compute: theta="<<theta[itheta]<<" Egamma= "<< Egamma[i]<<" sum="<<sum[i]<<endl;

    
  intensity[itheta]=sum;

     
 }//itheta




 // energy integration
 for (int itheta=0;itheta< theta.size();itheta++)
 {
  intensity_integrated_E[itheta]=0;

  for (int i =0;     i<Egamma.size(); i++) 
  for (int ii=i;    ii<Egamma.size();ii++) 
   intensity_integrated_E[itheta][i]+=intensity[itheta][ii]*Egamma[ii];


  intensity_integrated_E[itheta] *= log(Egamma[1]/Egamma[0]);
 }

 // theta integration
for (int itheta=0;itheta< theta.size();itheta++)
{
    intensity_integrated_theta  [itheta]=0;
    intensity_integrated_theta_E[itheta]=0;

  int theta_integration_method=2; //AWS20170106

  if(theta_integration_method==0)//AWS20170106
  for (int iitheta=0;iitheta<= itheta;iitheta++)
  {
    //AWS20170105 original method with correction for log binning
   double solid_angle;
   if(theta_grid_type=="linear")
          solid_angle = 2.0*pi*( cos(theta[iitheta]*dtr)-cos((theta[iitheta]    +dtheta)*dtr ) );

   if(theta_grid_type=="log")
          solid_angle = 2.0*pi*( cos(theta[iitheta]*dtr)-cos((theta[iitheta]*(1+dtheta))*dtr ) );

    intensity_integrated_theta  [itheta] +=intensity             [iitheta] * solid_angle;
    intensity_integrated_theta_E[itheta] +=intensity_integrated_E[iitheta] * solid_angle;
  }
 

  if(theta_integration_method==1)//AWS20170106
  for (int iitheta=1;iitheta<= itheta;iitheta++)
  {
    // better method using solid angle and average intensities. Assigned to upper theta boundaries, zero for itheta=0
    double solid_angle;
   
    solid_angle = 2.0*pi*( cos(theta[iitheta-1]*dtr) - cos((theta[iitheta])*dtr ) );

    intensity_integrated_theta  [itheta] += 0.5 * (intensity             [iitheta-1] + intensity             [iitheta]) * solid_angle;
    intensity_integrated_theta_E[itheta] += 0.5 * (intensity_integrated_E[iitheta-1] + intensity_integrated_E[iitheta]) * solid_angle;
  }


  if(theta_integration_method==2)//AWS20170106
  for (int iitheta=1;iitheta<= itheta;iitheta++)
  {
    // 2pi sin theta in integration function, more accurate since intensity roughly proportional to 1/theta.  Assigned to upper theta boundaries, zero for itheta=0

    intensity_integrated_theta  [itheta] += pi * (intensity             [iitheta-1] * sin (theta[iitheta-1] *dtr)  + intensity             [iitheta] * sin((theta[iitheta])*dtr )) 
                                               * (theta                 [iitheta]      -   theta[iitheta-1])*dtr ;
    
    intensity_integrated_theta_E[itheta] += pi * (intensity_integrated_E[iitheta-1] * sin (theta[iitheta-1] *dtr)  + intensity_integrated_E[iitheta] * sin((theta[iitheta])*dtr )) 
                                               * (theta                 [iitheta]      -   theta[iitheta-1])*dtr ;
  }



 } // itheta

 cout<<"<< SolarIC::compute"<<endl;
 return;
}

///////////////////////////////////////////////////////////////////////
void SolarIC::print()
{
 cout<<">> SolarIC::print"<<endl;

 for (int itheta=0;itheta< theta.size();itheta++)
 for (int i=0;     i     <Egamma.size();i++)
  cout<<  "theta="       <<theta[itheta]<<" Egamma= "<< Egamma[i]
      <<"  intensity="                   <<intensity                   [itheta][i]
      <<"  intensity_integrated_E="      <<intensity_integrated_E      [itheta][i]
      <<"  intensity_integrated_theta="  <<intensity_integrated_theta  [itheta][i]
      <<"  intensity_integrated_theta_E="<<intensity_integrated_theta_E[itheta][i]
      <<   endl;

 ///////////////////////////////////////////////////////////////////////
 // idl-compatible output for plotting
 ///////////////////////////////////////////////////////////////////////

   string  idltag = "; idl" ; // grep idl on output to get the idl commands only. text after ";" is ignored by idl

   int postscript=1;

   if(postscript==0) cout<<"set_plot,'X'" <<idltag<<endl;
   if(postscript==1) cout<<"set_plot,'ps'"<<idltag<<endl;
   if(postscript==1) cout<<"device, file='solar_IC_spectra_angles.ps'"<<idltag<<endl;
   if(postscript==1) cout<<"!p.font=0"<<idltag<<endl; // postscript fonts


   // spectra for  angles

    cout<<"Egamma=[";
     for (int i=0;     i     <Egamma.size()-1;i++)  cout<<Egamma[i]<<",";
                                                    cout<<Egamma[Egamma.size()-1]<<"]"
							<<idltag<<endl;

						   
    cout<<"Egamma=float(Egamma)"<<idltag<<endl; // in case interpreted by idl as integer !

 // specific angles

    if(postscript==0) cout<<"window,0"<<idltag<<endl;

    int ditheta=1; // increase to plot less spectra

    double thetaminplot=0.265; // mean solar angular radius
           thetaminplot=0.0;  // can increase to restrict spectra plotted
    double thetamaxplot=180.; // can   reduce to restrict spectra plotted

 for (int itheta=0;itheta< theta.size();itheta+=ditheta)
 {
     cout<<"theta="       <<theta[itheta]<<" "<<idltag<<endl;

 
     

     cout<<"intensity=[";
     for (int i=0;     i     <Egamma.size()-1;i++)  cout<<intensity                 [itheta][i]<<",";
                                                    cout<<intensity                 [itheta][Egamma.size()-1]<<"]"
							<<idltag<<endl;

    
     if(itheta==0)    cout<< "plot,egamma,intensity*egamma*egamma,/xlog,/ylog,yrange=[1e-7,1e-1],/nodata,xtitle='energy, MeV', ytitle='E!e2!n X intensity, MeV!e2!n cm!e-2!n sr!e-1!n s!e-1!n MeV!e-1!n',charsize=1.3"<<idltag<<endl; // typical intensity range

     if(  theta[itheta]>=thetaminplot &&   theta[itheta]<=thetamaxplot )   cout<<"oplot,egamma,intensity*egamma*egamma"<<"; plot spectrum for theta="<<theta[itheta] <<idltag<<endl;

     
    
 }



 // integrated angles

    if(postscript==0)  cout<<"window,1"<<idltag<<endl;
    if(postscript==1) cout<<"device, file='solar_IC_spectra_integrated_angles.ps'"<<idltag<<endl;

 for (int itheta=0;itheta< theta.size();itheta+=ditheta)
 {
     cout<<"theta="       <<theta[itheta]<<" "<<idltag<<endl;

     
     

     cout<<"intensity=[";
     for (int i=0;     i     <Egamma.size()-1;i++)  cout<<intensity_integrated_theta[itheta][i]<<",";
                                                    cout<<intensity_integrated_theta[itheta][Egamma.size()-1]<<"]"
							<<idltag<<endl;

    
 
     if(itheta==0)    cout<< "plot,egamma,intensity*egamma*egamma,/xlog,/ylog,yrange=[1e-7,1e-3],/nodata,xtitle='energy, MeV', ytitle='E!e2!n X intensity, MeV!e2!n cm!e-2!n sr!e-1!n s!e-1!n MeV!e-1!n',charsize=1.3"<<idltag<<endl; // typical intensity range

    if(  theta[itheta]>=thetaminplot &&   theta[itheta]<=thetamaxplot )     cout<<"oplot,egamma,intensity*egamma*egamma"<<"; plot spectrum for theta<"<<theta[itheta] <<idltag<<endl;

    
    
 }


  


  // profiles for energies

 if(postscript==0)  cout<<"window,2"<<idltag<<endl;
 if(postscript==1) cout<<"device, file='solar_IC_profiles_integrated_energies.ps'"<<idltag<<endl;

 cout<<"theta=[";
 for (int itheta=0;itheta< theta.size()-1;itheta++)cout<<theta[itheta]<<",";
                                                   cout<<theta[theta.size()-1]<<"]"
						       <<idltag<<endl;

 cout<<"theta=float(theta)"<<idltag<<endl; // in case interpreted by idl as integer !

 int diEgamma=1; // increase for less profiles

  for (int i=0;     i     <Egamma.size()-1;i+=diEgamma)
  {
   

   cout<<" intensity=[";
   for (int itheta=0;itheta< theta.size()-1;itheta++)
   cout<<intensity_integrated_E        [itheta][i]<<",";
   cout<<intensity_integrated_E[theta.size()-1][i]<<"]"
   <<idltag<<endl;
   cout<<"Egamma=float(Egamma)"<<idltag<<endl; // in case interpreted by idl as integer !

   cout<<"intensity=intensity + 1d-20"<<idltag<<endl; // to avoid zero on log plot

   if(i==0)cout<<" plot,theta,intensity,/xlog,/ylog,xrange=["<<theta_min<<","<<theta_max<<"],yrange=[1e-9,1e-3], xstyle=1,ystyle=1,xtitle='angle from sun, degrees', ytitle='integral intensity, cm!e-2!n sr!e-1!n s!e-1!n',charsize=1.3,/nodata "<<idltag<<endl; // xstyle=1 forces exact range

   cout<<"oplot,theta,intensity"  <<"; plot profile for Egamma > "<<Egamma[i] <<idltag<<endl;
  }  


 



 cout<<"end"<<idltag<<endl; // end of idl program


 cout<<"<< SolarIC::print"<<endl;

 return;
 }


///////////////////////////////////////////////////////////////////////
void SolarIC::write()
{
 cout<<">> SolarIC::write"<<endl;

  
  string outfile; 
  
  outfile = "!"+ FITS_output_filename; // "!" to overwrite existing file

  cout<<" creating output file "<<outfile<<endl;


  int status = 0; 
  
  fitsfile* fptr = 0;
 
  fits_create_file(&fptr, outfile.c_str(), &status);   /* create new file or overwrite existing one */

 /* Create the primary array image (64-bit double pixels */
  //just for test
/*
  int naxis=2; long naxes[2]; naxes[0]=10; naxes[1]=20;
  fits_create_img(fptr, DOUBLE_IMG, naxis, naxes, &status);
  fits_report_error(stderr, status);  
*/

  int tbltype=BINARY_TBL;
  LONGLONG naxis2=0; // initial number of rows, normally set to 0 as automatic


  
  int tfields=Egamma.size()+1;        // number of columns in table, profiles and 1 for angle
  char **ttype;                       // column names
  char **tform;                       // e.g. "1D" rE, rD  r=repeat count
  char **tunit;                       // e.g. "cm-2 sr-1 s-1 MeV-1";
  char extname[100];                  // extension name

  ttype=new char*[tfields];
  tform=new char*[tfields];
  tunit=new char*[tfields];

  int datatype=TDOUBLE;
  int colnum = 1;

  LONGLONG firstrow=1;
  LONGLONG firstelem=1;
  LONGLONG nelements; // number of rows
  
  double *array;

  //------------------------------------------------
  strcpy(extname,"Differential_intensity_profile");
  cout<<"writing FITS extension "<<extname<<endl;

 for (int i=0;i<tfields;i++)
 {

 if(i==0)
 {
 
  ttype[i]= new char[30];
  strcpy(ttype[i],"angle");

  tform[i]= new char[3]; strcpy(tform[i],"1D");
  tunit[i]= new char[30];strcpy(tunit[i],"degrees");

 }

 if(i>0)
 {
  char energy_string[20];
  sprintf(energy_string,"intensity_%i",i ); // FITS standard: no blanks
 
  ttype[i]= new char[30];
  strcpy(ttype[i],energy_string);

  tform[i]= new char[3]; strcpy(tform[i],"1D");
  tunit[i]= new char[30];strcpy(tunit[i],"cm-2 sr-1 s-1 MeV-1");
  }
 }

  naxis2=0;

  fits_create_tbl 
    (fptr, tbltype,  naxis2,  tfields, ttype, tform, tunit,  extname, &status);

  fits_report_error(stderr, status); 



  nelements=theta.size();// number of theta values


   array=new double[nelements];

   // angles in column 1

   for (int itheta=0;itheta<theta.size();itheta++)
      array[itheta]=theta[itheta];

   colnum=1;

   fits_write_col 
   (fptr,  datatype,  colnum,  firstrow, firstelem, nelements, array, &status);

   fits_report_error(stderr, status);

   // profiles start in column 2

  for (int iEgamma=0;iEgamma<Egamma.size();iEgamma++)
  {
    
    for (int itheta=0;itheta<theta.size();itheta++)
       array[itheta]=intensity[itheta][iEgamma];

   colnum=iEgamma+2;

   fits_write_col 
   (fptr,  datatype,  colnum,  firstrow, firstelem, nelements, array, &status);

   fits_report_error(stderr, status);  

  }

  //------------------------------------------------
  strcpy(extname,"Energy_integrated_profile");
  cout<<"writing FITS extension "<<extname<<endl;
 


 for (int i=0;i<tfields;i++)
 {

 if(i==0)
 {
 
  ttype[i]= new char[30];
  strcpy(ttype[i],"angle");

  tform[i]= new char[3]; strcpy(tform[i],"1D");
  tunit[i]= new char[30];strcpy(tunit[i],"degrees");

 }

 if(i>0)
 {
  char energy_string[20];
  sprintf(energy_string,"intensity_%i",i ); // FITS standard: no blanks
 
  ttype[i]= new char[30];
  strcpy(ttype[i],energy_string);

  tform[i]= new char[3]; strcpy(tform[i],"1D");
  tunit[i]= new char[30];strcpy(tunit[i],"cm-2 sr-1 s-1");
  }
 }

  naxis2=0;

  fits_create_tbl 
    (fptr, tbltype,  naxis2,  tfields, ttype, tform, tunit,  extname, &status);

  fits_report_error(stderr, status); 

  nelements=theta.size();// number of theta values


   array=new double[nelements];

  // angles in column 1

   for (int itheta=0;itheta<theta.size();itheta++)
      array[itheta]=theta[itheta];

   colnum=1;

   fits_write_col 
   (fptr,  datatype,  colnum,  firstrow, firstelem, nelements, array, &status);

   fits_report_error(stderr, status);

   // profiles start in column 2

  for (int iEgamma=0;iEgamma<Egamma.size();iEgamma++)
  {
    
    for (int itheta=0;itheta<theta.size();itheta++)
       array[itheta]=intensity_integrated_E[itheta][iEgamma];

   colnum=iEgamma+2;

   fits_write_col 
   (fptr,  datatype,  colnum,  firstrow, firstelem, nelements, array, &status);

   fits_report_error(stderr, status);  

  }


  //------------------------------------------------
  strcpy(extname,"Angle_integrated_profile");
  cout<<"writing FITS extension "<<extname<<endl;


 for (int i=0;i<tfields;i++)
 {

 if(i==0)
 {
 
  ttype[i]= new char[30];
  strcpy(ttype[i],"angle");

  tform[i]= new char[3]; strcpy(tform[i],"1D");
  tunit[i]= new char[30];strcpy(tunit[i],"degrees");

 }

 if(i>0)
 {
  char energy_string[20];
  sprintf(energy_string,"intensity_%i",i ); // FITS standard: no blanks
 
  ttype[i]= new char[30];
  strcpy(ttype[i],energy_string);

  tform[i]= new char[3]; strcpy(tform[i],"1D");
  tunit[i]= new char[30];strcpy(tunit[i],"cm-2 s-1 MeV-1"); //AWS20170105
  }
 }


  naxis2=0;

  fits_create_tbl 
    (fptr, tbltype,  naxis2,  tfields, ttype, tform, tunit,  extname, &status);

  fits_report_error(stderr, status); 

  nelements=theta.size();// number of theta values


   array=new double[nelements];

 // angles in column 1

   for (int itheta=0;itheta<theta.size();itheta++)
      array[itheta]=theta[itheta];

   colnum=1;

   fits_write_col 
   (fptr,  datatype,  colnum,  firstrow, firstelem, nelements, array, &status);

   fits_report_error(stderr, status);

   // profiles start in column 2

  for (int iEgamma=0;iEgamma<Egamma.size();iEgamma++)
  {
    
    for (int itheta=0;itheta<theta.size();itheta++)
       array[itheta]=intensity_integrated_theta[itheta][iEgamma];

   colnum=iEgamma+2;

   fits_write_col 
   (fptr,  datatype,  colnum,  firstrow, firstelem, nelements, array, &status);

   fits_report_error(stderr, status);  

  }

  //------------------------------------------------
  strcpy(extname,"Energy_and_angle_integrated_profile");
  cout<<"writing FITS extension "<<extname<<endl;


 for (int i=0;i<tfields;i++)
 {

 if(i==0)
 {
 
  ttype[i]= new char[30];
  strcpy(ttype[i],"angle");

  tform[i]= new char[3]; strcpy(tform[i],"1D");
  tunit[i]= new char[30];strcpy(tunit[i],"degrees");

 }

 if(i>0)
 {
  char energy_string[20];
  sprintf(energy_string,"intensity_%i",i ); // FITS standard: no blanks
 
  ttype[i]= new char[30];
  strcpy(ttype[i],energy_string);

  tform[i]= new char[3]; strcpy(tform[i],"1D");
  tunit[i]= new char[30];strcpy(tunit[i],"cm-2 s-1");
  }
 }

  naxis2=0;

  fits_create_tbl 
    (fptr, tbltype,  naxis2,  tfields, ttype, tform, tunit,  extname, &status);

  fits_report_error(stderr, status); 

  nelements=theta.size();// number of theta values


   array=new double[nelements];

   // angles in column 1

   for (int itheta=0;itheta<theta.size();itheta++)
      array[itheta]=theta[itheta];

   colnum=1;

   fits_write_col 
   (fptr,  datatype,  colnum,  firstrow, firstelem, nelements, array, &status);

   fits_report_error(stderr, status);

   // profiles start in column 2

  for (int iEgamma=0;iEgamma<Egamma.size();iEgamma++)
  {
    
    for (int itheta=0;itheta<theta.size();itheta++)
       array[itheta]=intensity_integrated_theta_E[itheta][iEgamma];

   colnum=iEgamma+2;

   fits_write_col 
   (fptr,  datatype,  colnum,  firstrow, firstelem, nelements, array, &status);

   fits_report_error(stderr, status);  

  }





  ///////////////////////
  // angles in columns //
  ///////////////////////




  //------------------------------------------------
  strcpy(extname,"Spectrum_for_angles");
  cout<<"writing FITS extension "<<extname<<endl;

  tfields=theta.size() + 1; // columns for angles + one for energy

  ttype=new char*[tfields];
  tform=new char*[tfields];
  tunit=new char*[tfields];




  for (int i=0;i<tfields;i++)
{

 if(i==0)
 {
  
 ttype[i]= new char[30];
 strcpy(ttype[i],"energy");

 tform[i]= new char[3]; strcpy(tform[i],"1D");
 tunit[i]= new char[30];strcpy(tunit[i],"MeV");
 }

 if(i>0)
 {
 char    theta_string[20];
 sprintf(theta_string,"intensity_%i",i);
 
 ttype[i]= new char[30];
 strcpy(ttype[i],theta_string);

 tform[i]= new char[3]; strcpy(tform[i],"1D");
 tunit[i]= new char[30];strcpy(tunit[i],"cm-2 sr-1 s-1 MeV-1");
 }

 }

  naxis2=0;

  fits_create_tbl 
    (fptr, tbltype,  naxis2,  tfields, ttype, tform, tunit,  extname, &status);

   fits_report_error(stderr, status); 

  if(status==BAD_TFIELDS)
  {
    cout<<"=============================================================================="<<endl;
    cout<<" status after fits_create_tbl="<<status<<endl;
    cout<<" Too many angle columns requested, "<<tfields<<" is beyond FITS limit of 999." <<endl;
    cout<<" Hence not writing "<<extname<<" or other angle column extensions."            <<endl;
    cout<<" Increase delta theta or increase range of theta, or use other extensions !"   <<endl;
    cout<<" Continuing to write remaining Energies and Angles extensions as usual."       <<endl; 
    cout<<"=============================================================================="<<endl;
   }



 if(status==0) // to write all the angle column extensions
 {

  nelements=Egamma.size();// number of energy values

  array=new double[nelements];
    
// energies column
  for (int itheta=0;itheta<theta.size();itheta++)
  {
   for (int iEgamma=0;iEgamma<Egamma.size();iEgamma++)
      array[iEgamma]=Egamma[iEgamma];

   colnum=1;

   fits_write_col 
   (fptr,  datatype,  colnum,  firstrow, firstelem, nelements, array, &status);

   fits_report_error(stderr, status);  

  }

// angles columns
  for (int itheta=0;itheta<theta.size();itheta++)
  {
   for (int iEgamma=0;iEgamma<Egamma.size();iEgamma++)
      array[iEgamma]=intensity[itheta][iEgamma];

   colnum=itheta+2;

   fits_write_col 
   (fptr,  datatype,  colnum,  firstrow, firstelem, nelements, array, &status);

   fits_report_error(stderr, status);  

  }


  //------------------------------------------------
  strcpy(extname,"Spectrum_times_Esquared_for_angles");
  cout<<"writing FITS extension "<<extname<<endl;

  tfields=theta.size() + 1; // columns for angles + one for energy

  ttype=new char*[tfields];
  tform=new char*[tfields];
  tunit=new char*[tfields];




  for (int i=0;i<tfields;i++)
{

 if(i==0)
 {
  
 ttype[i]= new char[30];
 strcpy(ttype[i],"energy");

 tform[i]= new char[3]; strcpy(tform[i],"1D");
 tunit[i]= new char[30];strcpy(tunit[i],"MeV");
 }

 if(i>0)
 {
 char    theta_string[20];
 sprintf(theta_string,"intensity_%i",i);
 
 ttype[i]= new char[30];
 strcpy(ttype[i],theta_string);

 tform[i]= new char[3]; strcpy(tform[i],"1D");
 tunit[i]= new char[30];strcpy(tunit[i],"MeV cm-2 sr-1 s-1");
 }

 }

  naxis2=0;

  fits_create_tbl 
    (fptr, tbltype,  naxis2,  tfields, ttype, tform, tunit,  extname, &status);

  fits_report_error(stderr, status); 

  nelements=Egamma.size();// number of energy values

  array=new double[nelements];
    
// energies column
  for (int itheta=0;itheta<theta.size();itheta++)
  {
   for (int iEgamma=0;iEgamma<Egamma.size();iEgamma++)
      array[iEgamma]=Egamma[iEgamma];

   colnum=1;

   fits_write_col 
   (fptr,  datatype,  colnum,  firstrow, firstelem, nelements, array, &status);

   fits_report_error(stderr, status);  

  }

// angles columns
  for (int itheta=0;itheta<theta.size();itheta++)
  {
   for (int iEgamma=0;iEgamma<Egamma.size();iEgamma++)
     array[iEgamma]=intensity[itheta][iEgamma] * pow(Egamma[iEgamma],2.0);

   colnum=itheta+2;

   fits_write_col 
   (fptr,  datatype,  colnum,  firstrow, firstelem, nelements, array, &status);

   fits_report_error(stderr, status);  

  }


  //------------------------------------------------
  strcpy(extname,"Spectrum_for_integrated_angles");
  cout<<"writing FITS extension "<<extname<<endl;

  tfields=theta.size() + 1; // columns for angles + 1 for energy

  ttype=new char*[tfields];
  tform=new char*[tfields];
  tunit=new char*[tfields];

  for (int i=0;i<tfields;i++)
{
if(i==0)
 {
  
 ttype[i]= new char[30];
 strcpy(ttype[i],"energy");

 tform[i]= new char[3]; strcpy(tform[i],"1D");
 tunit[i]= new char[30];strcpy(tunit[i],"MeV");
 }

 if(i>0)
 {
 char    theta_string[20];
 sprintf(theta_string,"intensity_%i",i);
 
 ttype[i]= new char[30];
 strcpy(ttype[i],theta_string);

 tform[i]= new char[3]; strcpy(tform[i],"1D");
 tunit[i]= new char[30];strcpy(tunit[i],"cm-2 s-1 MeV-1"); //AWS20170105
 }


 }

  naxis2=0;

  fits_create_tbl 
    (fptr, tbltype,  naxis2,  tfields, ttype, tform, tunit,  extname, &status);

  fits_report_error(stderr, status); 

  nelements=Egamma.size();// number of energy values

  array=new double[nelements];
    

// energies column
  for (int itheta=0;itheta<theta.size();itheta++)
  {
   for (int iEgamma=0;iEgamma<Egamma.size();iEgamma++)
      array[iEgamma]=Egamma[iEgamma];

   colnum=1;

   fits_write_col 
   (fptr,  datatype,  colnum,  firstrow, firstelem, nelements, array, &status);

   fits_report_error(stderr, status);  

  }

// angles columns

  for (int itheta=0;itheta<theta.size();itheta++)
  {
   for (int iEgamma=0;iEgamma<Egamma.size();iEgamma++)
      array[iEgamma]=intensity_integrated_theta[itheta][iEgamma]  ;

   colnum=itheta+2;

   fits_write_col 
   (fptr,  datatype,  colnum,  firstrow, firstelem, nelements, array, &status);

  fits_report_error(stderr, status); 

  }

 //------------------------------------------------
  strcpy(extname,"Spectrum_times_Esquared_for_integrated_angles");
  cout<<"writing FITS extension "<<extname<<endl;

  tfields=theta.size() + 1; // columns for angles + 1 for energy

  ttype=new char*[tfields];
  tform=new char*[tfields];
  tunit=new char*[tfields];

  for (int i=0;i<tfields;i++)
{
if(i==0)
 {
  
 ttype[i]= new char[30];
 strcpy(ttype[i],"energy");

 tform[i]= new char[3]; strcpy(tform[i],"1D");
 tunit[i]= new char[30];strcpy(tunit[i],"MeV");
 }

 if(i>0)
 {
 char    theta_string[20];
 sprintf(theta_string,"intensity_%i",i);
 
 ttype[i]= new char[30];
 strcpy(ttype[i],theta_string);

 tform[i]= new char[3]; strcpy(tform[i],"1D");
 tunit[i]= new char[30];strcpy(tunit[i],"MeV cm-2 s-1"); //AWS20170105
 }


 }

  naxis2=0;

  fits_create_tbl 
    (fptr, tbltype,  naxis2,  tfields, ttype, tform, tunit,  extname, &status);

  fits_report_error(stderr, status); 

  nelements=Egamma.size();// number of energy values

  array=new double[nelements];
    

// energies column
  for (int itheta=0;itheta<theta.size();itheta++)
  {
   for (int iEgamma=0;iEgamma<Egamma.size();iEgamma++)
      array[iEgamma]=Egamma[iEgamma];

   colnum=1;

   fits_write_col 
   (fptr,  datatype,  colnum,  firstrow, firstelem, nelements, array, &status);

   fits_report_error(stderr, status);  

  }

// angles columns

  for (int itheta=0;itheta<theta.size();itheta++)
  {
   for (int iEgamma=0;iEgamma<Egamma.size();iEgamma++)
      array[iEgamma]=intensity_integrated_theta[itheta][iEgamma] * pow(Egamma[iEgamma],2.0); ;

   colnum=itheta+2;

   fits_write_col 
   (fptr,  datatype,  colnum,  firstrow, firstelem, nelements, array, &status);

  fits_report_error(stderr, status); 

  }

  }// status==0 for angle columns limit


 ////////////////////////////////////////
 // energies and angles always written //
 ////////////////////////////////////////


  status = 0 ; // have to reset for further processing to avoid repeating the error status

  //------------------------------------------------
  strcpy(extname,"Energies");
  cout<<"writing FITS extension "<<extname<<endl;

  tfields=1; // one column
  
  ttype=new char*[tfields];
  tform=new char*[tfields];
  tunit=new char*[tfields];


 
 ttype[0]= new char[30];
 strcpy(ttype[0],"Energy");

 tform[0]= new char[3]; strcpy(tform[0],"1D");
 tunit[0]= new char[30];strcpy(tunit[0],"MeV");
 
 /* for testing
 ttype[1]= new char[30];
 strcpy(ttype[1],"testing");

 tform[1]= new char[3]; strcpy(tform[1],"1D");
 tunit[1]= new char[30];strcpy(tunit[1],"test");
 */

  naxis2=0;

  fits_create_tbl 
    (fptr, tbltype,  naxis2,  tfields, ttype, tform, tunit,  extname, &status);

  fits_report_error(stderr, status); 



  nelements=Egamma.size();// number of theta values


   array=new double[nelements];

  for (int iEgamma=0;iEgamma<Egamma.size();iEgamma++)
     array[iEgamma]=Egamma[iEgamma];

   colnum=1;

   fits_write_col 
   (fptr,  datatype,  colnum,  firstrow, firstelem, nelements, array, &status);

   fits_report_error(stderr, status);  

   /* for testing
  colnum=2;

   fits_write_col 
   (fptr,  datatype,  colnum,  firstrow, firstelem, nelements, array, &status);

   fits_report_error(stderr, status);  
   */
  
  //------------------------------------------------
  strcpy(extname,"Angles");
  cout<<"writing FITS extension "<<extname<<endl;

  tfields=1; // one column



 
 ttype[0]= new char[30];
 strcpy(ttype[0],"Angle");

 tform[0]= new char[3]; strcpy(tform[0],"1D");
 tunit[0]= new char[30];strcpy(tunit[0],"degrees");
 
  naxis2=0;

  fits_create_tbl 
    (fptr, tbltype,  naxis2,  tfields, ttype, tform, tunit,  extname, &status);

  fits_report_error(stderr, status); 

  nelements=theta.size();// number of theta values

  array=new double[nelements];

  for (int itheta=0;itheta<theta.size();itheta++)
     array[itheta]=theta[itheta];

   colnum=1;

   fits_write_col 
   (fptr,  datatype,  colnum,  firstrow, firstelem, nelements, array, &status);

   fits_report_error(stderr, status);  

  

  ////////////////////////////////////////////

  fits_close_file(fptr, &status);           
  fits_report_error(stderr, status);  


 cout<<"<< SolarIC::write"<<endl;
 return;
}
/////////////////////////////////////////

