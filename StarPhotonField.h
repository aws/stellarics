using namespace std;

#include <valarray>

class StarPhotonField
{
public:

  double T, Rstar;
  void   init(double T, double Rstar, int debug);

  valarray<double>  nph( valarray<double> Eph, double r);

  valarray<double>  nBB( valarray<double> Eph);

  void   test();

private:
 
  int    initialized;
  int    debug;
};
