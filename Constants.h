  double pi   = acos(-1.0);

  double k    = 1.380658e-16;          // Boltzmann's constant       erg/K 
  double h    = 6.6260755e-27;         // Planck's constant          erg s
  double hbar = h/(2.0*pi);

  double c    = 2.99792458e10;         // light velocity,            cm/s


  double me   = 0.51099906;            // electron rest mass,        MeV
  double re   = 2.8179409238e-13;      // classical electron radius, cm

  double Tsun = 5776.;                 // solar temperature,         K
  double Rsun =    6.95508e10;         // solar radius,              cm
  double AUcm =1.495978706e13;         // Astronomical unit,         cm 

  double MeV2erg = 1.6021773349e-6;    // MeV -> erg
