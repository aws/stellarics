using namespace std;
#include <valarray>
#include <vector>
#include <string>

class SolarIC
{
 public:
  double theta_min,theta_max,dtheta; // range of angles from star, and grid step, degrees
  string theta_grid_type;            // theta grid type: "linear" || "log"
  double r_min,r_max;                // range of distances from star, AU
  double s_min,s_max,ds;             // line-of-sight range and step, AU

  double Rstar;                      // radius of star,               cm
  double Tstar;                      // temperature of star,          K
  double d;                          // distance of star from earth,  AU

  valarray<double> theta;

  double Egamma_min,Egamma_max,Egamma_factor;
  valarray<double> Egamma;
  vector<valarray<double> > intensity;
  vector<valarray<double> > intensity_integrated_E;
  vector<valarray<double> > intensity_integrated_theta;
  vector<valarray<double> > intensity_integrated_theta_E;

  double Ee_min,Ee_max,Ee_factor;
  string             leptons_name;
  valarray<double>   leptons_parameters;
  valarray<double> Ee;
  

  double Eph_min,Eph_max,Eph_factor;
  valarray<double> Eph;

  int IC_emiss_method;

  string FITS_output_filename;

  void init(double Egamma_min, double Egamma_max, double Egamma_factor, 
            double     Ee_min, double     Ee_max, double Ee_factor,
            double    Eph_min, double    Eph_max, double Eph_factor,
            double  theta_min, double  theta_max, double     dtheta, string theta_grid_type,
            double      s_min, double      s_max, double     ds,
            double      r_min, double      r_max,
            double      Tstar, double      Rstar, double     d,   
            string   leptons_name,       valarray<double> leptons_parameters,
            int      IC_emiss_method,
            string   FITS_output_filename,
            int        debug);

  void compute();
  void print();
  void write();

 private:
 
  int debug;
};
