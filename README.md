Inverse Compton gamma-rays from the solar heliosphere and stars.

Originally public at
https://sourceforge.net/projects/stellarics/
and now continued on Max-Planck Society (MPCDF) gitlab for future developments.
 
2017 June 16:
This version is a copy of v5 from sourceforge.

=============================================================
The original README from sourceforge:

Solar Inverse Compton program.

Computes inverse Compton scattering from the solar heliosphere.
Can also be used for other stars.

Bug reports and  suggestions for further developments are welcome.

A. Strong, MPE, Garching
aws@mpe.mpg.de 

first release 05 Dec 2011 v1

updated       21 Feb 2012 v2

updated       10 Nov 2012 v3

updated       04 Dec 2013 v4 InverseCompton.cc : improved test case, rest unchanged

updated       07 Jan 2017 v5 SolarIC.cc: corrected angular integration for log angle grid, improved angular integration for log and linear grids. 

                             Corrected units for angle-integrated fluxes in FITS file.

Documentation
=============


The main program solar_ic_driver.cc is an example, to be edited to suit the users requirements.
It is more flexible than a parameter file, allows loops, dependence of parameters etc.
The parameters are described in the driver  file itself, and this should be sufficiently clear.

The output is a FITS file with multiple extensions for convenient use e.g. with fv plotting. These include profiles and spectra, both differential and integral. Should be self-explanatory.

idl-format commands are also written to the standard output, can be ignored or piped to output file if required. Produces quality plots of spectra at all angles and profiles for all integrated energies (from v3).
 

Other features:
* Function for Inverse Compton energy loss for a given photon field and electron energy (from v2).
* linear or logarithmic grid in angle from sun. Useful to resolve the rapid variation near sun while maintain full angular range to 180 deg (from v3)

Compilation.
===========


 Requires cfitsio v3.26 or later, available from http://heasarc.gsfc.nasa.gov/fitsio
Parallel execution using OpenMP is controlled with runtime scheduling. OpenMP is optional, if not required remove the openmp directive from the compilation.

Sample compilations:

setenv FITSIO_DIRECTORY /afs/ipp-garching.mpg.de/home/a/aws/propagate/c/cfitsio/3.26/gcc_sles11_olga2/cfitsio

GNU compiler (with OpenMP support, e.g. v4.3.4):

 g++ *.cc -O3 -fopenmp -lcfitsio -I$FITSIO_DIRECTORY/include -L$FITSIO_DIRECTORY/lib -o solar_ic_driver

to run:

setenv OMP_NUM_THREADS 16
setenv OMP_SCHEDULE "static"
./solar_ic_driver

Intel compiler:

 icpc *.cc -O3 -openmp -lcfitsio  -I$FITSIO_DIRECTORY/include -L$FITSIO_DIRECTORY/lib  -o solar_ic_driver

to run:

setenv OMP_NUM_THREADS 16
setenv OMP_SCHEDULE "auto"
./solar_ic_driver


User-defined models.
===================
The electron and positron spectra can be defined by the user by adding named models to the LeptonSpectrum class. The model parameters are freely definable. Several sample models are provided, including those based on existing publications. They will be regularly updated with new models.


Output.
====== 
The output FITS file contains multiple extensions for convenience of use and plotting.
A sample output is provided: solar_IC_example.fits

Definitions : I(E,theta)=differential intensity cm-2 sr-1 s-1 MeV-1 at angle theta from sun.
E= energy, MeV
theta=angle from sun, degrees

(The following text is best viewed after download in an editor for readability, not the default gitlab rendering of README.md).

Extension  Name                                             Contents           Rows     Columns   Units

                                                                                 variable = 
      
1          Differential_intensity_profile                   I( E, theta)       E        theta      cm-2 sr-1 s-1 MeV-1

2          Energy_integrated_profile                        I(>E, theta)       E        theta      cm-2 sr-1 s-1 

3          Angle_integrated_profile                         I( E,<theta)       E        theta      cm-2      s-1 MeV-1

4          Energy_and_angle_integrated_profile              I(>E,<theta)       E        theta      cm-2      s-1 

1-4                                                         Column 1 = theta


5          Spectrum_for_angles                              I( E, theta)       theta    E          cm-2 sr-1 s-1 MeV-1

6          Spectrum_times_Esquared_for_angles               I( E, theta)*E^2   theta    E          cm-2 sr-1 s-1 MeV

7          Spectrum_for_integrated_angles                   I( E,<theta)       theta    E          cm-2      s-1 MeV-1

8          Spectrum_times_Esquared_for_integrated_angles    I( E,<theta)*E^2   theta    E          cm-2      s-1 MeV

5-8                                                         Column 1 = E


9          Energies                                         E                  E                   MeV

10         Angles                                           theta              theta               degrees


The format is chosen to allow easy plotting with fv. For example to plot profiles >100 MeV, choose extension 2, and plot the appropriate column against column 1 (=angle). To plot a spectrum times E^2 for 0-20 degrees, choose extension 8 and plot the appropriate column against column 1 (= energy). Alternatively the FITS file can be read by any program using cfitsio, or output as ASCII using fv. Other operations like angular or energy ranges can be performed in fv using the Calculate tool: subtracting integral intensities in angle and/or energy.

idl output
==========
The program outputs idl commands tagged with "idl" for identification (from v3). 
To use this feature run as follows:
./solar_ic_driver |grep idl > plotresults.pro

idl
.run plotresults
exit

Three postscript files are produced with spectra for all angles and integrated angles on the grid and profiles for all integrated energies on the grid.
These are generated in SolarIC::print(), and this can easily be modified by the user to obtain other plots with different characteristics.

===================================
General information on this project
===================================

Description

A program to compute inverse Compton scattering from the solar heliosphere.
Cosmic ray electrons scatter on the photon fields around stars, including the sun,
to create gamma rays by the inverse Compton effect.
This program computes the spectrum and angular distribution of this emission.
The software also includes general-purpose routines for inverse Compton scattering 
on a given electron spectrum, for example for interstellar or astrophysical source modelling.




 
 The programme can also be used for other stars, and includes general-purpose routines 
 for inverse Compton scattering e.g. for interstellar emission.
 Package contains C++ routines, build instructions and description.
 Output is FITS table format, in various forms (spectra, profiles), and as idl commands.
 Includes inverse Compton energy loss routines for any given radiation field,
 including dE/dt and mean fractional energy loss, and checks against analytical formulae.
 Includes an option for a logarithmic grid in angle from the sun to better resolve the
 rapid variation near the sun while still maintaining the full angular range to 180 degrees.
 idl plotting output produces quality plots for all angles and energies.
 Included is a document with sample spectra and profiles, and a sample output FITS file 
 and a sample idl output program for the plots.
 
 The file IC_documentation.pdf contains derivations of the Inverse Compton formulae used
 and useful approximations.
 
 This software is the basis for the Fermi-LAT solar inverse Compton background model.

 For further information see these publications:

 http://arxiv.org/abs/astro-ph/0607563 ,
 
 http://arxiv.org/abs/0801.2178 ,
 
 http://arxiv.org/abs/1104.2093 ,
 
 http://adsabs.harvard.edu/abs/2013NuPhS.239..266O 
 
 and this conference presentation: https://confluence.slac.stanford.edu/download/attachments/130879399/poster_symposium_sun_2012.pdf

 and the corresponding conference paper http://arxiv.org/abs/1307.6798

 2017 June 16: now available from gitlab https://gitlab.mpcdf.mpg.de/aws/stellarics

 which will be used for future developments. 
 
 2018 May 4: IC_documentation.pdf, tex : derivation of inverse Compton formula for anisotropic case
 based on AWS thesis http://www.mpe.mpg.de/~aws/publications/Thesis.pdf 


See also https://gitlab.mpcdf.mpg.de/aws/galprop


