\documentclass[12pt]{article}
\usepackage{graphicx}
\begin{document}
\title{Inverse Compton: sun, stars and interstellar}
\author{A. W. Strong, MPE}
\maketitle

\section{Inverse Compton (IC) from the Sun and Stars}


This document generated on \today.

Documentation of {\it Stellarics} software, with examples and derivation of formulae.
Available from \begin{verbatim}https://gitlab.mpcdf.mpg.de/aws/stellarics \end{verbatim}
which also has references.
More information can be found in README and in the driver programe provided.
This will also be included here in future.

\section{Examples}
These examples were generated using  linear and logarithmic intervals of the angle $\theta$ from the sun.
The graphics were generated with the $idl$ commands output at run time.

The profiles show the decrease at the solar limb due to occultation of gamma rays from beyond the sun, and also the effect of the anisotropy of the solar radiation field, which reduces to almost zero the emission between the earth and the sun, due to IC scattering at very small angles. In contrast, the emission at small angles outside the disk includes the head-on scattering from beyond 1 AU, which is maximum. Hence the emission here is dominated by the distant regions.

%AWS 20180507: ps files need latex but want pdflatex now so converted them.

\begin{figure}[!h]
\includegraphics[width=1.0\textwidth, angle=0] {solar_IC_spectra_angles_0point3_5_to_40.pdf} %remove dot or error
\caption{Spectra for various angles from sun: $0.3^o,5^o,10^o,15^o,20^o,25^o,30^o,35^o,40^o$}
\end{figure}

% included 20180507, was commented, very dense
\begin{figure}[!h]
\includegraphics[width=1.0\textwidth, angle=0] {solar_IC_spectra_angles_example1.pdf}
\caption{Spectra for various angles from sun}
\end{figure}

% included 20180507, was commented, very dense
\begin{figure}[!h]
\includegraphics[width=1.0\textwidth, angle=0] {solar_IC_spectra_integrated_angles_example1.pdf}
\caption{Spectra for various integrated angles from sun}
\end{figure}

% included 20180507, was commented, OK
\begin{figure}[!h]
\includegraphics[width=1.0\textwidth, angle=0] {solar_IC_profiles_integrated_energies_example1.pdf}
\caption{Angular profiles for various integral energies}
\end{figure}

\begin{figure}[!h]
\includegraphics[width=1.0\textwidth, angle=0] {solar_IC_profiles_integrated_energies_10_100_1000_10000MeV.pdf}
\caption{Angular profiles for various integral energies}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{IC physical formulae}

Derivations from A.W. Strong PhD thesis 1975,
\begin{verbatim}
 http://www.mpe.mpg.de/~aws/publications/Thesis.pdf
\end{verbatim}

\subsection{Anisotropic IC}



\def\epsone{\epsilon_1}
\def\epstwo{\epsilon_2}
\def\epsonep{\epsilon_1^\prime}
\def\epstwop{\epsilon_2^\prime}
\def\Ee{E_e}
\def\me{m_e}
\def\etap{\eta^\prime}

Define

$\epsilon_1$ = target photon energy in lab

$\epsilon_2$ = gamma-ray energy in lab

$\epsilon_1^\prime$ = target photon energy in electron system

$\epsilon_2^\prime$ = gamma-ray energy in electron system

$\Ee$ = electron energy

$\eta$ = scatter angle of photon in lab

$\etap$ = scatter angle of photon in electron system


$${d\sigma_{KN}\over d\Omega}
={{r_e}^2\over2} ({\epstwop\over\epsonep})^2
[
 {\epstwop\over\epsonep} + {\epsonep\over\epstwop}   
 -\sin^2\etap
]
$$
$$\epstwop={\epsonep\over  1 + {\epsonep\over\me}(1 - \cos\etap)}$$

$$d\Omega=2\pi \sin\etap d\etap = 2\pi d\cos\etap$$

$$d\epstwop=  {\epsonep\over\me}{\epsonep\over [ 1 + {\epsonep\over\me}(1 - \cos\etap)]^2} d\cos\etap=
 {\epsonep\over\me}{(\epstwop)^2\over\epsonep} d\cos\etap $$

$${ d\cos\etap\over d\epstwop}={\me\over(\epstwop)^2} $$
$${d\sigma_{KN}\over d\epstwop}={d\sigma_{KN}\over d\Omega}{d\Omega\over d\cos\etap} { d\cos\etap\over d\epstwop} 
=2\pi{\me\over(\epstwop)^2} {d\sigma_{KN}\over d\Omega} $$

$${d\sigma_{KN}\over d\epstwop}
=\pi{r_e}^2  {\me\over(\epsonep)^2}
[
 {\epstwop\over\epsonep} + {\epsonep\over\epstwop}   
 -\sin^2\etap
]
$$


$$\epstwo=\gamma\epstwop(1 - \cos\etap)$$


$$(1 - \cos\etap)={\me\over\epsonep}({\epsonep\over\epstwop}    -1)$$

$${\epstwo\over\Ee}= \gamma\epstwop(1 - \cos\etap) {1\over\gamma\me}   
=\epstwop{\me\over\epsonep}({\epsonep\over\epstwop}    -1) {1\over\me}
= 1 - {\epstwop\over\epsonep}  
 $$
 


$${d\sigma_{KN}\over d\epstwo}={d\sigma_{KN}\over d\epstwop}{ d\epstwop\over d\epstwo}
={d\sigma_{KN}\over d\epstwop}{\epsonep\over\Ee}
$$

$${d\sigma_{KN}\over d\epstwo}=
{\epsonep\over\Ee}
\pi{r_e}^2  {\me\over(\epsonep)^2}
[
 {\epstwop\over\epsonep} + {\epsonep\over\epstwop}   
 -\sin^2\etap
]
=\pi{r_e}^2  {\me\over\epsonep\Ee}
[
 {\epstwop\over\epsonep} + {\epsonep\over\epstwop}   
 -\sin^2\etap
]
$$


$$\epsonep=\gamma\epsone(1 - \cos\eta)$$

$${dR\over d\epstwo} = n(\epsone) c ( 1 - \cos\eta) {d\sigma_{KN}\over d\epstwo}
 = n(\epsone) c {\epsonep\over\gamma\epsone} {d\sigma_{KN}\over d\epstwo}
$$

$${dR\over d\epstwo} = n(\epsone) c ( 1 - \cos\eta) {d\sigma_{KN}\over d\epstwo}
 = n(\epsone) c {\epsonep\over\gamma\epsone} 
\pi{r_e}^2  {\me\over\epsonep\Ee}
[
 {\epstwop\over\epsonep} + {\epsonep\over\epstwop}   
 -\sin^2\etap
]
$$

$${dR\over d\epstwo} = 
  n(\epsone) c  
\pi{r_e}^2  {\me\over\gamma\epsone\Ee}
[
 {\epstwop\over\epsonep} + {\epsonep\over\epstwop}   
 -\sin^2\etap
]
$$

$${dR\over d\epstwo} = 
  n(\epsone) c  
\pi{r_e}^2  {\me^2\over\epsone\Ee^2}
[
 {\epstwop\over\epsonep} + {\epsonep\over\epstwop}   
 -\sin^2\etap
]
$$

Define
$$v = {\epstwo\over\Ee}$$

$${\epstwop\over\epsonep} = 1 -  {\epstwo\over\Ee} = 1 - v $$

$$1 - \cos\etap={\me\over\epsonep}[{1\over 1-v}    -1] =
{\me\over\epsonep}{v\over 1-v}    $$
$$-\sin^2\etap=\cos^2\etap-1=[1- {\me\over\epsonep}{v\over 1-v}]^2-1
= ({\me\over\epsonep})^2({v\over 1-v})^2 - 2{\me\over\epsonep}{v\over 1-v}
   $$


$${d\sigma_{KN}\over d\epstwo}=\pi{r_e}^2
 {\me\over\epsonep\Ee}
[
({\me\over\epsonep})^2({v\over 1-v})^2 - 2{\me\over\epsonep}{v\over 1-v}
+ (1 - v) + {1\over 1 - v}
 ]
$$



$${dR\over d\epstwo} = n(\epsone) c \pi{r_e}^2
{\me^2\over\epsone\Ee^2}
[
({\me\over\epsonep})^2({v\over 1-v})^2 - 2{\me\over\epsonep}{v\over 1-v}
+ (1 - v) + {1\over 1 - v}
 ]$$

To make it look like Moskalenko\&Strong, ApJ 528, 763 (2000)  (MS2000),  equation 8:

$${dR\over d\epstwo} = n(\epsone) c \pi{r_e}^2
{\me^2\over\epsone\Ee^2(1-v)^2}
[
({\me\over\epsonep})^2 v^2 - 2{\me\over\epsonep}v(1 - v)
+ (1 - v)^3 +  1 - v
 ]$$


$${dR\over d\epstwo} = n(\epsone) c \pi{r_e}^2
{\me^2\over\epsone(\Ee-\epstwo)^2}
[
({\me\over\epsonep})^2 v^2 - 2{\me\over\epsonep}v(1 - v)
+ (1 - v)^3 +  1 - v
 ]$$

$${dR\over d\epstwo} = n(\epsone) c \pi{r_e}^2
{\me^2\over\epsone(\Ee-\epstwo)^2}
[
({\me\over\epsonep})^2 v^2 - 2{\me\over\epsonep}v(1 - v)
+ 2 - 4v + 3v^2 - v^3 
 ]$$

$${dR\over d\epstwo} = n(\epsone) c \pi{r_e}^2
{\me^2\over\epsone(\Ee-\epstwo)^2}
[
 2 - 2v({\me\over\epsonep} +2) + v^2( ({\me\over\epsonep})^2 + {2\me\over\epsonep}  +3 ) - v^3 
 ]$$

This is the MS2000 form  with
$$v = {\epstwo\over\gamma\me}$$
and 
$$\zeta=-\eta$$


The present derivation is more direct and avoids use of delta functions, and expresses the result in terms of the 
energy transfer $v$ and dimensionless units, in a less clumsy form, with the physical units separated.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Gamma-ray emissivity -  $\delta$-function approximation to IC cross-section}
%\def\Eg{E_\gamma}
\def\Eg{\epstwo}     % for consistency with above
\def\Ee{E_e    }
%\def\Eo{E_o    }
\def\Eo{\epsone}     % for consistency with above
\def\me{m_e    }
\def\nph{n_{ph}    }

Simple isotropic low-energy power-laws case:

$$I(\Eg)={1\over4\pi}\int q(\Eg)ds$$
emissivity
$$q(\Eg)d\Eg= N(\Ee)d\Ee \nph\sigma_T c $$

$$q(\Eg)= N(\Ee){d\Ee\over d\Eg}  \nph\sigma_T c $$

$$\Eg={4\over3}\gamma^2\Eo$$
$$\Eg={4\over3}({\Ee\over\me})^2\Eo$$
$$d\Eg={4\over3}. 2{\Ee\over\me^2}d\Ee \Eo $$
$${d\Ee\over d\Eg} ={3\over8}. {\me^2\over\Ee}{1\over\Eo} $$

$$q(\Eg)= N(\Ee){3\over8}. {\me^2\over\Ee}{1\over\Eo}  \nph\sigma_T c $$


$$ N(\Ee)=A\Ee^{-p}$$

$$q(\Eg)=A\Ee^{-p} {3\over8}. {\me^2\over\Ee}{1\over\Eo}  \nph\sigma_T c $$
$$q(\Eg)=A\Ee^{-(p+1)} {3\over8}. {\me^2\over\Eo}  \nph\sigma_T c $$
$$\Ee=({3\over4}{\Eg\me^2\over\Eo})^{1/2}$$

$$q(\Eg)=A({3\over4}{\Eg\me^2\over\Eo}) ^{-(p+1)/2} {3\over8} {\me^2\over\Eo}  \nph\sigma_T c $$

$$q(\Eg)=A({3\over4}{\me^2\over\Eo}) ^{-(p+1)/2} {3\over8} {\me^2\over\Eo}  \nph\sigma_T c    \ \       \Eg ^{-(p+1)/2} $$

$$q(\Eg)={A\over2}({3\over4}{\me^2\over\Eo}) ^{-(p-1)/2}  \nph\sigma_T c    \ \       \Eg ^{-(p+1)/2} $$

$$q(\epstwo)={A\over2}({3\over4}{\me^2\over\Eo}) ^{-(p-1)/2}  \nph\sigma_T c    \ \       \Eg ^{-(p+1)/2} $$
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Planck spectrum - $\delta$-function approximation to IC cross-section}

$$ \nph(\epsone) = {1\over \pi^2 \hbar^3 c^3}{ \epsone^2\over e^{\epsone/kT} - 1 }$$

$$q(\epstwo)=\int{A\over2}({3\over4} {\me^2\over\epsone}) ^{-(p-1)/2}  \nph(\epsone)\sigma_T c    \ \  \epstwo ^{-(p+1)/2} d\epsone $$

$$q(\epstwo)=\sigma_T c    \ \  \epstwo ^{-(p+1)/2} {A\over2}({3\over4} \me^2) ^{-(p-1)/2}\int \epsone^{(p-1)/2} \nph(\epsone) d\epsone $$

$$q(\epstwo)=\sigma_T c    \ \  \epstwo ^{-(p+1)/2} {A\over2}({3\over4} \me^2) ^{-(p-1)/2}
     {1\over \pi^2 \hbar^3 c^3}       \int { \epsone^{2 +{(p-1)/2}}  \over e^{\epsone/kT} - 1 }    d\epsone $$

For $p=3$

$$q(\epstwo)=\sigma_T c    \ \  \epstwo ^{-(p+1)/2} {A\over2}({3\over4} \me^2) ^{-(p-1)/2}
     {1\over \pi^2 \hbar^3 c^3} w_{ph}$$

where $ w_{ph} = \int\epsone\nph(\epsone) d\epsone$ = energy density

Define
$$y={\epsone/kT} $$

$$\epsone=kTy$$ 

$$q(\epstwo)=\sigma_T c    \ \  \epstwo ^{-(p+1)/2} {A\over2}({3\over4} \me^2) ^{-(p-1)/2}
     {1\over \pi^2 \hbar^3 c^3}       \int { (kTy)^{2 +{(p-1)/2}}  \over e^y - 1 } kT   dy $$

$$q(\epstwo)=\sigma_T c    \ \  \epstwo ^{-(p+1)/2} {A\over2}({3\over4} \me^2) ^{-(p-1)/2}
     {1\over \pi^2 \hbar^3 c^3}    (kT)^{3 +{(p-1)/2}}     \int { y^{2 +{(p-1)/2}}  \over e^y - 1 }   dy $$

For $p=3$
$$ \int { y^3  \over e^y - 1 }   dy ={ \pi^4\over 15}$$

$$q(\epstwo)=\sigma_T c    \ \  \epstwo ^{-2} {A\over2}({3\over4} \me^2) ^{-1}
     {1\over \pi^2 \hbar^3 c^3}    (kT)^4  { \pi^4\over 15}$$
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Planck spectrum - low-energy approximation to IC cross-section}
Blumenthal and Gould (1970) eq 2.43, 2.44, 2.45

$${dR\over d\epstwo}=8\pi {r_o}^2 c {1\over 4\epsone\gamma^2} \nph(\epsone) (2 x \ln x +x+1-2x^2)$$
$$x={\epstwo\over 4\gamma^2\epsone}, \ \ 0<x<1$$

$${dR\over d\epstwo}={8\pi {r_o}^2 c \over \epstwo} \nph(\epsone)  (2 x \ln x +x+1-2x^2)x $$

$$q(\epstwo)=\int_{\epsone}
  \int_{x=0}^{x=1}
{1\over \pi^2 \hbar^3 c^3}{ \epsone^2\over e^{\epsone/kT} - 1 } 
 {8\pi {r_o}^2 c             \over \epstwo} 
 (2 x \ln x +x+1-2x^2)x d\epsone N(\Ee)d\Ee  $$



$$q(\epstwo)={8\pi {r_o}^2 c \over \epstwo} 
\int_{\epsone}{1\over \pi^2 \hbar^3 c^3}{ \epsone^2\over e^{\epsone/kT} - 1 } d\epsone
  \int_{x=0}^{x=1}
 (2 x \ln x +x+1-2x^2)x  N(\Ee)d\Ee  $$

$$ N(\Ee)=A\Ee^{-p}$$

$$\Ee=\gamma m=({\epstwo\over 4\epsone x})^{1\over2} m$$

$$d\Ee={1\over2} ({\epstwo\over 4\epsone})^{1\over2}x^{-3/2} m dx$$

$$  \int_{x=0}^{x=1}
 (2 x \ln x +x+1-2x^2)x  N(\Ee)d\Ee$$
$$ =\int_{x=0}^{x=1}
 (2 x \ln x +x+1-2x^2)x A  ({\epstwo\over 4\epsone x})^{-{p\over2}} m^{-p}{1\over2} ({\epstwo\over 4\epsone})^{1\over2}x^{-3/2} m dx  $$

$$ ={A\over2} m^{-p+1}\int_{x=0}^{x=1} (2 x \ln x +x+1-2x^2)x ({\epstwo\over 4\epsone x})^{-{p\over2}}  ({\epstwo\over 4\epsone})^{1\over2}x^{-3/2}  dx$$

$$ ={A\over2} m^{-p+1}({\epstwo\over 4\epsone })^{-{p-1\over2}}\int_{x=0}^{x=1} (2 x \ln x +x+1-2x^2)  x^{p-1\over2}    dx$$

$$q(\epstwo)={8\pi {r_o}^2 c \over \epstwo} 
\int_{\epsone}{1\over \pi^2 \hbar^3 c^3}{ \epsone^2\over e^{\epsone/kT} - 1 } d\epsone
  {A\over2} m^{-p+1}({\epstwo\over 4\epsone })^{-{p-1\over2}}\int_{x=0}^{x=1} (2 x \ln x +x+1-2x^2)  x^{p-1\over2}    dx$$

$$q(\epstwo)={8\pi {r_o}^2 c  {A\over2} m^{-p+1}
 \epstwo^{-{p+1\over2}}} 
\int_{\epsone}{1\over \pi^2 \hbar^3 c^3}{ \epsone^2\over e^{\epsone/kT} - 1 } d\epsone
  ({1\over 4\epsone })^{-{p-1\over2}}\int_{x=0}^{x=1} (2 x \ln x +x+1-2x^2)  x^{p-1\over2}    dx$$



$$q(\epstwo)= \epstwo^{-{p+1\over2}}
{8\pi {r_o}^2 c  {A\over2} m^{-p+1}\int_{x=0}^{x=1} (2 x \ln x +x+1-2x^2)  x^{p-1\over2}    dx
} 
\int_{\epsone}{1\over \pi^2 \hbar^3 c^3}{ \epsone^2\over e^{\epsone/kT} - 1 } d\epsone
  ({4\epsone })^{{p-1\over2}}$$

$$q(\epstwo)= \epstwo^{-{p+1\over2}}
{8\pi {r_o}^2 c  {A\over2} m^{-p+1} 4 ^{{p-1\over2}}
\int_{x=0}^{x=1} (2 x \ln x +x+1-2x^2)  x^{p-1\over2}    dx
} 
\int_{\epsone}{1\over \pi^2 \hbar^3 c^3}{ \epsone^{2+{p-1\over2}}\over e^{\epsone/kT} - 1 } d\epsone
  $$


\end{document}
