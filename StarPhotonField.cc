using namespace std;

#include <iostream>
#include <valarray>
#include <cmath>


#include "StarPhotonField.h"

void   StarPhotonField::init(double T_, double Rstar_, int debug_)

{
T=T_;
Rstar=Rstar_;
debug=debug_;
initialized=1;

 cout<<" StarPhotonField: T="<<T<<" Rstar="<<Rstar<<endl;

return;
}


////////////////////////////////////////////////////////////////////////////////
valarray<double> StarPhotonField::nph(valarray<double> Eph, double r)
{
valarray<double> result;
result.resize(Eph.size());

result=0.5*nBB(Eph)*(1.0 - sqrt(1.0-(Rstar*Rstar)/(r*r) ) );

 if(debug==1) 
 for(int i=0;i<Eph.size();i++) cout<<"StarPhotonField::nph "<<Eph[i]<<" result=" <<result[i]<<" "<<endl;
return result;
}

////////////////////////////////////////////////////////////////////////////////
valarray<double> StarPhotonField::nBB(valarray<double> Eph)
{
  // Planck Black Body cm-3 MeV-1

#include"Constants.h"


 double constant= 1.0/( pow(pi,2.0)*pow(hbar,3.0)*pow(c,3.0) );  //cm-3 erg-1
  //  cout<<"constant="<<constant<<endl;

valarray<double> result;
result.resize(Eph.size());

 valarray<double> Eph_erg=Eph*MeV2erg;

 result=constant * pow(Eph_erg,2.0)/(exp(Eph_erg/(k*T))-1.0) * MeV2erg; //cm-3 MeV-1

 if(debug==1) 
 for(int i=0;i<Eph.size();i++) cout<<"StarPhotonField::nBB Eph="<<Eph[i]<<" Eph_erg="<<Eph_erg[i]<<" result=" <<result[i]<<" "<<endl;

return result;
}

//////////////////////////////////////////////////

void StarPhotonField::test()
{

  cout<<" StarPhotonField::test()"<<endl;

  double T=5776.;  // solar temperature
  //          T=2.7; // test with CMB
  double Rstar=6.95508e10;// solar radius cm
  int    debug=1;

 StarPhotonField starphotonfield;
 starphotonfield.init(T,Rstar,debug);
 cout<<" StarPhotonField: T="<<starphotonfield.T<<" Rstar="<<starphotonfield.Rstar<<endl;

double r=1.0e11;

 int nEph=120;
 valarray<double> Eph_grid(nEph),nBB_grid(nEph),nph_grid(nEph);
 

 for(int i=0;i<Eph_grid.size();i++) Eph_grid[i]=1.0e-4*pow(10,i*.1)*1e-6;// MeV

 cout<<"testing nBB"<<endl;
 nBB_grid=starphotonfield.nBB  (Eph_grid);

 cout<<"testing nph, r="<<r<<endl;
 nph_grid=starphotonfield.nph  (Eph_grid,r);

 for(int i=0;i<Eph_grid.size();i++) cout<<"test: Eph_grid="<<Eph_grid[i]<<" nBB_grid="<<nBB_grid[i]<<" nph_grid="<<nph_grid[i]<<" "<<endl;


return ;
}
/////////////////////////////////////////
/*
int main()
{
 StarPhotonField starphotonfield;
 starphotonfield.test();
 return 0;
}
*/
