using namespace std;
#include<vector>
#include <valarray>
#include <string>

class LeptonSpectrum
{
public:
  void             init(string name, valarray<double> parameters, int debug);
  valarray<double> fluxe(valarray<double> Ee, double r);
  
  void   test();

  string name;
  valarray<double> parameters; 
  
private:
  
  int    initialized;
  int    debug;
};
