/*
Solar Inverse Compton program. 

Computes inverse Compton scattering from the solar heliosphere.
Can also be used for other stars.

Bug reports and  suggestions for further developments are welcome.

A. Strong, MPE, Garching
aws@mpe.mpg.de 
 5 Dec 2011 first release  v1
21 Feb 2012 v2
10 Nov 2012 second release v3
04 Dec 2013 v4
07 Jan 2017 v5


Documentation
=============
For more details see README in this directory.

This driver is an example, to be edited to suit the users requirements.
It is more flexible than a parameter file, allows loops, dependence of parameters etc.

The output is a FITS file with multiple extensions for convenient use e.g. with fv plotting. These include profiles and spectra, both differential and integral. Should be self-explanatory.

idl-format commands are also written to the standard output, can be ignored or piped to output file if required. Produces quality plots of spectra for all angles and profiles for all integrated energies.
 



Compilation.
===========


 Requires cfitsio v3.26 or later, available from http://heasarc.gsfc.nasa.gov/fitsio
OpenMP is controlled with runtime scheduling.

Sample compilations:

setenv FITSIO_DIRECTORY /afs/ipp-garching.mpg.de/home/a/aws/propagate/c/cfitsio/3.26/gcc_sles11_olga2/cfitsio

GNU compiler (with OpenMP support, e.g. v4.3.4):

 g++ *.cc -O3 -fopenmp -lcfitsio -I$FITSIO_DIRECTORY/include -L$FITSIO_DIRECTORY/lib -o solar_ic_driver

to run:

setenv OMP_NUM_THREADS 16
setenv OMP_SCHEDULE "static"
./solar_ic_driver

Intel compiler:

 icpc *.cc -O3 -openmp -lcfitsio  -I$FITSIO_DIRECTORY/include -L$FITSIO_DIRECTORY/lib  -o solar_ic_driver

to run:

setenv OMP_NUM_THREADS 16
setenv OMP_SCHEDULE "auto"
./solar_ic_driver


User-defined models.
===================
The electron and positron spectra can be defined by the user by adding named models to the LeptonSpectrum class. The model parameters are freely definable. Several sample models are provided, including those based on existing publications. They will be regularly updated with new models.

*/

/////////////////////////////////////////////////////////////////////////////

using namespace std;
#include<iostream>
#include<valarray>
#include<string>



#include"StarPhotonField.h"
#include"InverseCompton.h"
#include"LeptonSpectrum.h"
#include"SolarIC.h"

int main()
{
#include"Constants.h"

cout<<">>solar_ic_driver"<<endl;

  int testSolarIC        =1; // 0 or 1
  int testStarPhotonField=0; // 0 or 1
  int testInverseCompton =0; // 0 or 1
  int testLeptonSpectrum =0; // 0 or 1


 if (testSolarIC==1)
 {
  SolarIC solaric;
  // gamma-ray energy range MeV, and grid factor
  double Egamma_min   = 1.0e0;//1.0e0 ;
  double Egamma_max   = 1.0e6;//1.0e6;
  double Egamma_factor=  pow(10,0.2);//pow(10,0.1);

  // electron energy range MeV, and grid factor
  double Ee_min       = 1.0e3;//1.0e2;
  double Ee_max       = 1.0e6;//1.0e7;
  double Ee_factor    = 1.05;

  // target photon range MeV, and grid factor
  double Eph_min      =  0.1*1.0e-6;
  double Eph_max      = 10.0*1.0e-6;
  double Eph_factor   = 1.05;

  // angle from sun range and grid spacing, degrees
  double theta_min    = 0.2;//0.3  ;//0.2;// 0.265; // solar radius 0.26-0.27 deg  0.26->0  0.27>0    0.265->0
  double theta_max    = 180.;//45.;//180;
  double  dtheta      = 0.02;//0.5;//0.02; linear: grid spacing (deg), log: grid factor=1+dtheta       //0.075 linear:gives idl problem, too large array.
  string theta_grid_type="log"; // "linear" or "log"
  
  // test linear grid for angle integration AWS20170107 
  /*
         theta_grid_type="linear";        //AWS20170107
          dtheta        = 0.5;            //AWS20170107
  */  


  // integration range from Sun, AU
  double   r_min      = Rsun/AUcm;
  double   r_max      = 2.0;//10.0;

  // integration range and step from earth, AU
  double   s_min      = 0.001;
  double   s_max      = r_max + 1.0; // make sure to reach r_max in all directions
  double  ds          = 0.010;// 0.005; // smaller values for more accuracy, larger values for more speed

  // these are values for the Sun, but can be set to values for any star. 

  double Tstar = Tsun;            // solar temperature, K
  double Rstar = Rsun;            // solar radius,      cm
  double d     = 1.0;             // Sun is at 1.0 AU

  string             leptons_name;
  valarray<double>   leptons_parameters(10);

  int IC_emiss_method = 2;

  string FITS_output_filename = "solar_IC_leptons6_phi0.fits";
         FITS_output_filename = "solar_IC_test.fits";


  int lepton_selection=6;

  if(lepton_selection==1)        leptons_name= "powerlaw";

  if(lepton_selection==2)        leptons_name= "Fermi2011_electrons";
  if(lepton_selection==3)        leptons_name= "Fermi2011_positrons";
  if(lepton_selection==4)        leptons_name= "Fermi2011_electrons_positrons";
  if(lepton_selection==5)        leptons_name= "Fermi2011_electrons_positrons_modulated1";
  if(lepton_selection==6)        leptons_name= "Fermi2011_electrons_positrons_modulated2";

  if(lepton_selection==7)        leptons_name= "imos_electrons_modulated1";
  if(lepton_selection==8)        leptons_name= "imos_electrons_modulated2";
  if(lepton_selection==9)        leptons_name= "imos_electrons_modulated3";

    leptons_parameters[0]=0.;// 400.; // Phi0 e.g. 400.
    leptons_parameters[1]=1.0 ; // constant
    leptons_parameters[2]=3.0 ; // spectral index  

  if(lepton_selection==2 || lepton_selection==3 || lepton_selection==4 || lepton_selection==5 || lepton_selection==6) 
  {  
   leptons_parameters[1]=3.e3;// Ee_break
   leptons_parameters[2]=1.5; // ge1=electron index below break
   leptons_parameters[3]=5.e3;// Ep_break
   leptons_parameters[4]=2.0; // gp1=positron index below break

   leptons_parameters[5]=1.0;//  electrons factor
   leptons_parameters[6]=1.0;//  positrons factor
  }
  

 int debug = 0;

  solaric.init(Egamma_min,Egamma_max,Egamma_factor,
                   Ee_min,    Ee_max,    Ee_factor,
                  Eph_min,    Eph_max,  Eph_factor,
	        theta_min,  theta_max,      dtheta, theta_grid_type,
	            s_min,      s_max,      ds,  
	            r_min,      r_max, 
	            Tstar,      Rstar,      d,     
               leptons_name,
               leptons_parameters,
               IC_emiss_method,
               FITS_output_filename,
               debug);            

  solaric.compute();

  int print_results =1; 
  if (print_results==1)
  solaric.print();// print results to standard output including idl format. also useful for testing. optional.

  solaric.write();// write to FITS. recommended format.

 }


 // tests of individual functions (for development)

if (testStarPhotonField==1)
{
 StarPhotonField starphotonfield;
 starphotonfield.test();
}

if (testInverseCompton==1)
{
 InverseCompton inversecompton;

 int options;

     options=0; // test sigma KN        
 inversecompton.test(options);

     options=0; // test emissivity
 inversecompton.test(options);

     options=3; // test energy losses
 inversecompton.test(options);

     options=1; // test sigma KN        
 inversecompton.test(options);
}

if (testLeptonSpectrum==1)
{
 LeptonSpectrum leptonspectrum;
 leptonspectrum.test();
}

cout<<"<<solar_ic_driver"<<endl;

  return 0;
}
