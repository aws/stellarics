using namespace std;
#include <vector>
#include <valarray>
#include <string>
#include <iostream>
#include "LeptonSpectrum.h"


void LeptonSpectrum::init(string name_,valarray<double> parameters_, int debug_)
{
  cout<<">>LeptonSpectrum::init"<<endl;
  name=name_;
  parameters.resize(parameters_.size());
  parameters=parameters_;

  debug=debug_;
  initialized=1;

  cout<<"name of lepton spectrum :" <<name;

  cout<<"  parameters: ";
  for(int i=0;i<parameters.size();i++) cout<<parameters[i]<<" " ; cout<<endl;

  cout<<"<<LeptonSpectrum::init"<<endl;
  return;
}
///////////////////////////////
valarray<double> LeptonSpectrum::fluxe(valarray<double> Ee, double r)
{
  if(debug==1)cout<<">>LeptonSpectrum::fluxe name="<<name<<endl;

  valarray<double> result(Ee.size());
//////////////////////////////////////////////////////////////////
  if(name=="powerlaw")
  {
   double constant=parameters[1];
   double ge      =parameters[2];
   result=constant*pow(Ee,-ge);
  }
//////////////////////////////////////////////////////////////////
 if(name=="Fermi2011_electrons")
 { // Ackermann etal Fermi-LAT arXiv:1109.052 electrons fit, converting from GeV m^2 to MeV cm^2 
  double ge=3.19;
  result = 2.07e-9* pow(Ee/2.e4, -ge);
 }
//////////////////////////////////////////////////////////////////
 if(name=="Fermi2011_positrons")
 { // Ackermann etal Fermi-LAT arXiv:1109.052 positrons fit, converting from GeV m^2 to MeV cm^2 
  double ge=2.77;
  result = 2.02e-10* pow(Ee/2.e4, -ge);
 }

 //////////////////////////////////////////////////////////////////
 if(name=="Fermi2011_electrons_positrons")
 { // Ackermann etal Fermi-LAT arXiv:1109.052 electrons and positrons fits summed, converting from GeV m^2 to MeV cm^2
 
   double Ee_break=parameters[1];// electron break energy
   double ge1     =parameters[2];// electron index below break
   double Ep_break=parameters[3];// positron break energy
   double gp1     =parameters[4];// positron index below break

   double electrons_factor=parameters[5]; // electrons factor relative to reference
   double positrons_factor=parameters[6]; // positrons factor relative to reference

   if (debug==1) cout<<"Ee_break="<<Ee_break<<" ge1="<<ge1<<" Ep_break="<<Ep_break<<" gp1="<<gp1<<endl;

   valarray<double> electrons(Ee.size());
   valarray<double> positrons(Ee.size());

  // electrons
  double ge=3.19;
  electrons  = 2.07e-9 * pow(Ee/2.e4, -ge);


  for(int i=0;i<Ee.size();i++)  if (Ee[i]<Ee_break)
   {            electrons[i] *= pow(Ee[i]/Ee_break, -(ge1-ge));
     if (debug==1)
      cout<<Ee[i]<<" electron energy below break "<<" factor="<<pow(Ee[i]/Ee_break, -(ge1-ge))
          <<" electrons="<<electrons[i]<<endl;
   }

  // positrons
  double gp=2.77;
  positrons= 2.02e-10* pow(Ee/2.e4, -gp);

  for(int i=0;i<Ee.size();i++)  if (Ee[i]<Ep_break)
   {            positrons[i] *= pow(Ee[i]/Ep_break, -(gp1-gp));
     if (debug==1)
      cout<<Ee[i]<<" positron energy below break "<<" factor="<<pow(Ee[i]/Ep_break, -(gp1-gp))
          <<" positrons="<<positrons[i]<<endl;
   }

  result = electrons * electrons_factor + positrons * positrons_factor;
 }

 /////////////////////////////////////////////////////////////////

if(name=="Fermi2011_electrons_positrons_modulated1")
 { // Ackermann etal Fermi-LAT arXiv:1109.052 electrons and positrons fits summed, converting from GeV m^2 to MeV cm^2 
   // the ubiquituous force-field approximation
   // expressions from Moskalenko et al. ApJ 652, L65 (2006), Abdo etal 2011 ApJ 734, 116 (2011) equations (2) and (3)
   // Phi1: cycles 20/22  Phi2:cycle 21
 
   
   double Phi0    =parameters[0];

   double Phi1,Phi2;
   double rb=100.;
   double r0=10.;

   if(r>=r0)Phi1=Phi0/1.88*          (pow(r,-0.4)-pow(rb,-0.4)); 
   if(r< r0)Phi1=Phi0/1.88*(0.24+8.0*(pow(r,-0.1)-pow(r0,-0.1)));

   double Phi=Phi1-Phi0; // since using spectrum at 1AU, modulation is relative to this

  if(debug==1)cout<<"leptons name="<<name<<" r="<<r<<" Phi="<<Phi<<endl;




   double Ee_break=parameters[1];// electron break energy
   double ge1     =parameters[2];// electron index below break
   double Ep_break=parameters[3];// positron break energy
   double gp1     =parameters[4];// positron index below break

   double electrons_factor=parameters[5]; // electrons factor relative to reference
   double positrons_factor=parameters[6]; // positrons factor relative to reference

   if (debug==1) cout<<"Ee_break="<<Ee_break<<" ge1="<<ge1<<" Ep_break="<<Ep_break<<" gp1="<<gp1<<endl;

   valarray<double> electrons(Ee.size());
   valarray<double> positrons(Ee.size());

   valarray<double> Eeprime=Ee+Phi;

  // electrons
  double ge = 3.19;
  electrons = 0.;
  //  electrons  = 2.07e-9 * pow(Eeprime/2.e4, -ge);

  // electron mininum energy applied everywhere, required r>1AU where Eeprime<Ee.
  for(int i=0;i<Ee.size();i++)    if (Eeprime[i]>Ee[0]) electrons[i]  = 2.07e-9 * pow(Eeprime[i]/2.e4, -ge);

  for(int i=0;i<Ee.size();i++)    if (Eeprime[i] < Ee_break && Eeprime[i]>Ee[0])
    {            electrons[i] *= pow( Eeprime[i] / Ee_break, -(ge1-ge));

     if (debug==1)
       cout<<" Ee="<<Ee[i]<< " Phi="<<Phi<<" electron energy + Phi below break="<<Eeprime[i]
           <<" factor="<<pow(Eeprime[i]/Ee_break, -(ge1-ge))
           <<" electrons="<<electrons[i]<<endl;
   }

  // positrons
  double gp = 2.77;
  positrons = 0;
  //  positrons= 2.02e-10* pow(Eeprime /2.e4, -gp);

  for(int i=0;i<Ee.size();i++)    if (Eeprime[i]>Ee[0])   positrons[i]= 2.02e-10* pow(Eeprime[i] /2.e4, -gp);

  for(int i=0;i<Ee.size();i++)    if (Eeprime[i] < Ep_break && Eeprime[i]>Ee[0] )
    {            positrons[i] *= pow( Eeprime[i] / Ep_break, -(gp1-gp));

     if (debug==1)
       cout<<" Ee="<<Ee[i]<< " Phi="<<Phi<<" positron energy + Phi below break="<<Eeprime[i]
           <<" factor="<<pow(Eeprime[i]/Ep_break, -(gp1-gp))
           <<" positrons="<<positrons[i]<<endl;
   }

  result = electrons * electrons_factor + positrons * positrons_factor;



  result *= pow(Ee/(Ee+Phi),2.0);
 }

////////////////////////////////////////////////////////////////

if(name=="Fermi2011_electrons_positrons_modulated2")
 { // Ackermann etal Fermi-LAT arXiv:1109.052 electrons and positrons fits summed, converting from GeV m^2 to MeV cm^2 
   // the ubiquituous force-field approximation
   // expressions from Moskalenko et al. ApJ 652, L65 (2006), Abdo etal 2011 ApJ 734, 116 (2011) equations (2) and (3)
   // Phi1: cycles 20/22  Phi2:cycle 21
 
   double Phi0    =parameters[0];

   double Phi1,Phi2;
   double rb=100.;
   double r0=10.;



  Phi2=Phi0     *          (pow(r,-0.1)-pow(rb,-0.1))/(1.0-pow(rb,-0.1));

  double Phi=Phi2-Phi0; // since using spectrum at 1AU, modulation is relative to this

  if(debug==1)cout<<"leptons name="<<name<<" r="<<r<<" Phi="<<Phi<<endl;




   double Ee_break=parameters[1];// electron break energy
   double ge1     =parameters[2];// electron index below break
   double Ep_break=parameters[3];// positron break energy
   double gp1     =parameters[4];// positron index below break

   double electrons_factor=parameters[5]; // electrons factor relative to reference
   double positrons_factor=parameters[6]; // positrons factor relative to reference

   if (debug==1) cout<<"Ee_break="<<Ee_break<<" ge1="<<ge1<<" Ep_break="<<Ep_break<<" gp1="<<gp1<<endl;

   valarray<double> electrons(Ee.size());
   valarray<double> positrons(Ee.size());

   valarray<double> Eeprime=Ee+Phi;

  // electrons
  double ge = 3.19;
  electrons = 0.;
  //  electrons  = 2.07e-9 * pow(Eeprime/2.e4, -ge);

  // electron mininum energy applied everywhere, required r>1AU where Eeprime<Ee.
  for(int i=0;i<Ee.size();i++)    if (Eeprime[i]>Ee[0]) electrons[i]  = 2.07e-9 * pow(Eeprime[i]/2.e4, -ge);

  for(int i=0;i<Ee.size();i++)    if (Eeprime[i] < Ee_break && Eeprime[i]>Ee[0])
    {            electrons[i] *= pow( Eeprime[i] / Ee_break, -(ge1-ge));

     if (debug==1)
       cout<<" Ee="<<Ee[i]<< " Phi="<<Phi<<" electron energy + Phi below break="<<Eeprime[i]
           <<" factor="<<pow(Eeprime[i]/Ee_break, -(ge1-ge))
           <<" electrons="<<electrons[i]<<endl;
   }

  // positrons
  double gp = 2.77;
  positrons = 0;
  //  positrons= 2.02e-10* pow(Eeprime /2.e4, -gp);

  for(int i=0;i<Ee.size();i++)    if (Eeprime[i]>Ee[0])   positrons[i]= 2.02e-10* pow(Eeprime[i] /2.e4, -gp);

  for(int i=0;i<Ee.size();i++)    if (Eeprime[i] < Ep_break && Eeprime[i]>Ee[0] )
    {            positrons[i] *= pow( Eeprime[i] / Ep_break, -(gp1-gp));

     if (debug==1)
       cout<<" Ee="<<Ee[i]<< " Phi="<<Phi<<" positron energy + Phi below break="<<Eeprime[i]
           <<" factor="<<pow(Eeprime[i]/Ep_break, -(gp1-gp))
           <<" positrons="<<positrons[i]<<endl;
   }

  result = electrons * electrons_factor + positrons * positrons_factor;


  result *= pow(Ee/(Ee+Phi),2.0);
 }
///////////////////////////////////////////////////////////////

if(name=="imos_electrons_modulated1")
 { 
    
   // electron interstellar spectrum as in   Abdo etal 2011 ApJ 734, 116 (2011) equation (4)
   // modulation as in Abdo etal 2011 ApJ 734, 116 (2011) equations (2) and (3)
   // Phi1: cycles 20/22  Phi2:cycle 21
   // NB   only valid for Phi=400 !
   
   double Phi0    =parameters[0];

   double Phi1,Phi2;
   double rb=100.;
   double r0=10.;

   if(r>=r0)Phi1=Phi0/1.88*          (pow(r,-0.4)-pow(rb,-0.4)); 
   if(r< r0)Phi1=Phi0/1.88*(0.24+8.0*(pow(r,-0.1)-pow(r0,-0.1)));

   double Phi=Phi1; // since using spectrum outside heliosphere

  if(debug==1)cout<<"leptons name="<<name<<" r="<<r<<" Phi="<<Phi<<endl;

  double a =160.24;
  double b =  7.0 ;
  double c = -1.2 ;
  double d =  3.03;
  
  valarray<double> EeGeVprime=(Ee+Phi)/1e3;
                                                    result    =  a * pow(b+c, -d) * pow(EeGeVprime/b,    -3.0);
 for(int i=0;i<Ee.size();i++) if(EeGeVprime[i] >=b) result[i] =  a *                pow(EeGeVprime[i]+c, -d  );

 
 

  result*=1.0e-7; // m^-2 GeV^-1 -> cm-2 MeV-1
  result *= pow(Ee/(Ee+Phi),2.0);

 if(debug==1)
   for(int i=0;i<Ee.size();i++) cout<<"imos model 1 r="<<r
   <<" Ee="<<Ee[i]<<" result="<<  result[i]<<" "<<result[i]*1e7*pow(Ee[i]/1e3,3.0)<<endl;

 }
///////////////////////////////////////////////////////////////

if(name=="imos_electrons_modulated2")
 { 
    
   // electron interstellar spectrum as in   Abdo etal 2011 ApJ 734, 116 (2011) equation (4)
   // modulation as in Abdo etal 2011 ApJ 734, 116 (2011) equations (2) and (3)
   // Phi1: cycles 20/22  Phi2:cycle 21
   // NB   only valid for Phi=400 !
   
   double Phi0    =parameters[0];

   double Phi1,Phi2;
   double rb=100.;
   double r0=10.;

   Phi2 = Phi0 * (pow(r,-0.1)-pow(rb,-0.1))/(1.0-pow(rb,-0.1));

   double Phi=Phi2; // since using spectrum outside heliosphere

  if(debug==1)cout<<"leptons name="<<name<<" r="<<r<<" Phi="<<Phi<<endl;

  double a =160.24;
  double b =  7.0 ;
  double c = -1.2 ;
  double d =  3.03;
  
 

 valarray<double> EeGeVprime=(Ee+Phi)/1e3;
                                                    result    =  a * pow(b+c, -d) * pow(EeGeVprime/b,    -3.0);
 for(int i=0;i<Ee.size();i++) if(EeGeVprime[i] >=b) result[i] =  a *                pow(EeGeVprime[i]+c, -d  );


 

  result*=1.0e-7; // m^-2 GeV^-1 -> cm-2 MeV-1
  result *= pow(Ee/(Ee+Phi),2.0);

 if(debug==1)
   for(int i=0;i<Ee.size();i++) cout<<"imos model 2 r="<<r
   <<" Ee="<<Ee[i]<<" result="<<  result[i]<<" "<<result[i]*1e7*pow(Ee[i]/1e3,3.0)<<endl;
 }

///////////////////////////////////////////////////////////////

if(name=="imos_electrons_modulated3")
 { 
    
   // electron interstellar spectrum as in   Abdo etal 2011 ApJ 734, 116 (2011) equation (4)
   // modulation as in Abdo etal 2011 ApJ 734, 116 (2011) equations (2) and (3)
   // Phi1: cycles 20/22  Phi2:cycle 21
   // model 3: Phi1 for r> 1 AU, constant r<1 AU
   // NB   only valid for Phi=400 !
 
   
   double Phi0    =parameters[0];

   double Phi1,Phi2;
   double rb=100.;
   double r0=10.;

   
   if(r>=r0)Phi1=Phi0/1.88*          (pow(r,-0.4)-pow(rb,-0.4)); 
   if(r< r0)Phi1=Phi0;

   double Phi=Phi1; // since using spectrum outside heliosphere
   

  if(debug==1)cout<<"leptons name="<<name<<" r="<<r<<" Phi="<<Phi<<endl;

  double a =160.24;
  double b =  7.0 ;
  double c = -1.2 ;
  double d =  3.03;

  
  valarray<double> EeGeVprime=(Ee+Phi)/1e3;
                                                    result    =  a * pow(b+c, -d) * pow(EeGeVprime/b,    -3.0);
 for(int i=0;i<Ee.size();i++) if(EeGeVprime[i] >=b) result[i] =  a *                pow(EeGeVprime[i]+c, -d  );

 

  result *= 1.0e-7;                 // m^-2 GeV^-1 -> cm-2 MeV-1
  result *= pow(Ee/(Ee+Phi),2.0);   // modulation

  if(debug==1)
   for(int i=0;i<Ee.size();i++) cout<<"imos model 3 r="<<r
   <<" Ee="<<Ee[i]<<" result="<<  result[i]<<" "<<result[i]*1e7*pow(Ee[i]/1e3,3.0)<<endl;


 }
///////////////////////////////////////////////////////////////////

 if(name=="Fermi2011_linear")
 { // Ackermann etal Fermi-LAT arXiv:1109.052 electrons fit, converting from GeV m^2 to MeV cm^2 
  double ge=3.19;
  result = 2.07e-9* pow(Ee/2.e4, -ge);
  result *= r; // r in AU so linear increase
 }
 /////////////////////////////////////////////////////////////////

  if(debug==1)
    for(int i=0;i<Ee.size();i++) cout<<"LeptonSpectrum: name="<<name<<" Phi0="<<parameters[0]<<" r="<<r<<" Ee= "<<Ee[i]<<" result="<<result[i]<<endl;

  if(debug==1)  cout<<"<<LeptonSpectrum::fluxe"<<endl;

  return result;
}
//////////////////////////////

void LeptonSpectrum::test()
{
  cout<< "LeptonSpectrum::test"<<endl;

  LeptonSpectrum leptonspectrum;
  double Ee_min=1.0;
  double Ee_max=1e6;
  double Ee_factor=pow(10.,0.5);

  int     n_grid = int(log(Ee_max/Ee_min)/log(Ee_factor)+ 1.001) ;
  valarray<double> Ee;
  valarray<double> fluxe;
  Ee.resize(n_grid);
  fluxe.resize(n_grid);

  for(int i=0;i<Ee.size();i++) Ee[i]= Ee_min*pow(Ee_factor,i);
  cout<<"electron grid size="<<Ee.size()<<endl;
  cout<<"Ee= ";    for(int i=0;i<Ee.size();i++)    cout<<    Ee[i]<<" " ; cout<<endl;

  string name="powerlaw";
  valarray<double> parameters;
  parameters.resize(10);
  
  double constant=1.0;
  double ge      =3.0;
  double Phi0    =500.;

  parameters[0]=Phi0;
  parameters[1]=constant;
  parameters[2]=ge;

  int debug=1;
  leptonspectrum.init(name,parameters,debug);

  double r=0.5;
  fluxe=leptonspectrum.fluxe(Ee,r);

  for(int i=0;i<Ee.size();i++) cout<<  "Ee= "<<  Ee[i]<<" fluxe="<<fluxe[i]<<endl;


  name="Fermi2011_electrons_positrons_modulated1";
  leptonspectrum.init(name,parameters,debug);

   r=0.5;
  fluxe=leptonspectrum.fluxe(Ee,r);

  for(int i=0;i<Ee.size();i++) cout<<  "Ee= "<<  Ee[i]<<" fluxe="<<fluxe[i]<<endl;

  name="Fermi2011_electrons_positrons";
  parameters[0]=Phi0;
  parameters[1]=3.e3;// Ee_break
  parameters[2]=1.0; // ge1=electron index below break
  parameters[3]=5.e3;// Ep_break
  parameters[4]=2.0; // gp1=positron index below break

  parameters[5]=1.0;//  electrons factor
  parameters[6]=1.0;//  positrons factor


  leptonspectrum.init(name,parameters,debug);

   r=1.0;
  fluxe=leptonspectrum.fluxe(Ee,r);

  for(int i=0;i<Ee.size();i++) cout<<  "Ee= "<<  Ee[i]<<" fluxe="<<fluxe[i]<<endl;

  name="Fermi2011_electrons_positrons_modulated1";
  leptonspectrum.init(name,parameters,debug);

  
  fluxe=leptonspectrum.fluxe(Ee,r);

  for(int i=0;i<Ee.size();i++) cout<<  "Ee= "<<  Ee[i]<<" fluxe="<<fluxe[i]<<endl;

  name="Fermi2011_electrons_positrons_modulated2";
  leptonspectrum.init(name,parameters,debug);

  fluxe=leptonspectrum.fluxe(Ee,r);

  for(int i=0;i<Ee.size();i++) cout<<  "Ee= "<<  Ee[i]<<" fluxe="<<fluxe[i]<<endl;

  r=2.0;
  name="Fermi2011_electrons_positrons";
  leptonspectrum.init(name,parameters,debug);
  fluxe=leptonspectrum.fluxe(Ee,r);

  for(int i=0;i<Ee.size();i++) cout<<  "Ee= "<<  Ee[i]<<" fluxe="<<fluxe[i]<<endl;

  name="Fermi2011_electrons_positrons_modulated1";
  leptonspectrum.init(name,parameters,debug);
  fluxe=leptonspectrum.fluxe(Ee,r);

  for(int i=0;i<Ee.size();i++) cout<<  "Ee= "<<  Ee[i]<<" fluxe="<<fluxe[i]<<endl;

  name="Fermi2011_electrons_positrons_modulated2";
  leptonspectrum.init(name,parameters,debug);
  fluxe=leptonspectrum.fluxe(Ee,r);

  for(int i=0;i<Ee.size();i++) cout<<  "Ee= "<<  Ee[i]<<" fluxe="<<fluxe[i]<<endl;

 return;
}
